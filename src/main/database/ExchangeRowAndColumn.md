###行列互换

下面是学生的成绩表（表名score，列名：学号、课程号、成绩）
![src](src.png)

使用sql实现将该表行转列为下面的表结构
![dest](dest.png)


```
##此处作答
select 学号, 
sum(case when 课程号=0001 then 成绩 else 0 end) as 课程号0001, 
sum(case when 课程号=0002 then 成绩 else 0 end) as 课程号0002, 
sum(case when 课程号=0003 then 成绩 else 0 end) as 课程号0003
from score group by 学号;



```