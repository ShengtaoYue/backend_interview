package datastruct;

/**
 * 给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组
 */
public class SortArray {
    public int[] solution(int[] nums1, int[] nums2) {
        int i = nums1.length - 1;
        int j = nums2.length - 1;
        int newLen = i + j - 1;
        while(i >= 0 && j >= 0)
        {
            if(nums1[i] >= nums2[j]) {
                nums1[newLen--] = nums1[i--];
            }else {
                nums1[newLen--] = nums2[j--];
            }
        }
        while(j >= 0) {
            nums1[newLen--] = nums2[j--];
        }
        return nums1;
    }
}
