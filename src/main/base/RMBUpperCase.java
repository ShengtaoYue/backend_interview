package base;

/**
 * 转人民币大写
 * e.g. 12.34 -> 拾贰元叁角肆分
 *      123456789.00 -> 壹亿贰仟叁佰肆拾伍万陆仟柒佰捌拾玖元整
 */
public class RMBUpperCase {

    public static String[] num = {"零","壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    public static String[] unit = {"", "拾", "佰", "仟"};
    public static StringBuilder stringBuilder = new StringBuilder();

    public String solution(String money) throws Exception {
        if (money == null || "".equals(money)) {
            return "零元整";
        }
        Integer index = money.lastIndexOf(".");
        String zhengshu = money.substring(0, index);
        String xiaoshu = money.substring(index + 1, money.length());
        //小数大于两位抛出异常
        if (xiaoshu.length() > 2) {
            throw new Exception("输入整数格式异常");
        } else if (xiaoshu.length() == 0 || (xiaoshu.length() == 1 && xiaoshu.charAt(0) == '0') || (xiaoshu.length() == 2 && xiaoshu.charAt(0) == '0' && xiaoshu.charAt(1) == '0' )) {
            convert(zhengshu);
            stringBuilder.append("整");
        } else if (xiaoshu.length() == 1 && xiaoshu.charAt(0) != '0') {
            convert(zhengshu);
            stringBuilder.append(num[Integer.parseInt(String.valueOf(xiaoshu.charAt(0)))] + "角");
        } else if (xiaoshu.length() == 2 ) {
            convert(zhengshu);
            stringBuilder.append(num[Integer.parseInt(String.valueOf(xiaoshu.charAt(0)))] + "角");
            stringBuilder.append(num[Integer.parseInt(String.valueOf(xiaoshu.charAt(1)))] + "分");
        }
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    //大小写转换
    public static void convert(String zheng) {
        String zhengshu[] = zheng.split("");

        int tmp = 0; //用来记录该取哪个计量单位
        int zeroNum = 0; //记录从右到左第5到第8位0的个数

        //数字为亿级
        if (zhengshu.length > 8) {
            for (int i = 0; i < zhengshu.length - 8; i++) {
                int number = Integer.parseInt(zhengshu[i]);

                //判断相邻是否为0
                if (zhengshu[i].equals("0") && zhengshu[i + 1].equals("0") && i + 1 < zhengshu.length - 8) {
                    continue;
                }

                //若0在亿级的最后一位则不输出
                if (i == zhengshu.length - 8 - 1 && zhengshu[zhengshu.length - 8 - 1].equals("0")) {
                    break;
                }
                //若中间有相邻的0则输出最后一个0
                if (zhengshu[i].equals("0")) {
                    stringBuilder.append(num[number]);
                } else {
                    stringBuilder.append(num[number] + unit[zhengshu.length - 8 - 1 - i]);
                }
            }
            stringBuilder.append("亿");
            //打印万级数字
            for (int i = zhengshu.length - 8; i < zhengshu.length - 4; i++) {
                int nums = Integer.parseInt(zhengshu[i]);

                if (zhengshu[i].equals("0") && zhengshu[i + 1].equals("0") && i + 1 < zhengshu.length - 4) {
                    tmp++;
                    zeroNum++;
                    continue;
                }

                if (i == zhengshu.length - 5 && zhengshu[zhengshu.length - 5].equals("0")) {
                    break;
                }
                if (zhengshu[i].equals("0")) {
                    stringBuilder.append(num[nums]);
                } else {
                    stringBuilder.append(num[nums] + unit[3 - tmp]);
                }
                tmp++;
            }
            if (zeroNum != 3) {
                stringBuilder.append("万");
            }
            tmp = 0;
            underWan(zhengshu,tmp);
            stringBuilder.append("元");
            tmp = 0;

        } else if (zhengshu.length > 4) {  //数字为万级
            for (int i = 0; i < zhengshu.length - 4; i++) {
                int nums = Integer.parseInt(zhengshu[i]);
                //判断万级数字相邻是否为零
                if (zhengshu[i].equals("0") && zhengshu[i + 1].equals("0") && i + 1 < zhengshu.length - 4) {
                    continue;
                }
                //万级最后一位为零，结束
                if (i == zhengshu.length - 4 - 1 && zhengshu[zhengshu.length - 4 - 1].equals("0")) {
                    break;
                }
                //中间相邻数字为零的情况
                if (zhengshu[i].equals("0")) {
                    stringBuilder.append(num[nums]);
                } else {
                    stringBuilder.append(num[nums] + unit[zhengshu.length - 4 - 1 - i]);
                }
            }
            stringBuilder.append("万");
            //打印低四位
            underWan(zhengshu,tmp);
            stringBuilder.append("元");
            tmp = 0;
        } else if (zhengshu.length <= 4) {
            for (int i = 0; i < zhengshu.length; i++) {
                int nums = Integer.parseInt(zhengshu[i]);

                if (i + 1 < zhengshu.length && zhengshu[i].equals("0") && zhengshu[i + 1].equals("0")) {
                    continue;
                }

                if (i == zhengshu.length - 1 && zhengshu[zhengshu.length - 1].equals("0")) {
                    break;
                }

                if (zhengshu[i].equals("0")) {
                    stringBuilder.append(num[nums]);
                } else {
                    stringBuilder.append(num[nums] + unit[zhengshu.length - 1 - i]);
                }
            }
            stringBuilder.append("元");
        }
    }
    //数字为亿级或者万级，打印低四位
    public static void underWan(String [] zhengshu,int tmp){
        for (int i = zhengshu.length - 4; i < zhengshu.length; i++) {
            int nums = Integer.parseInt(zhengshu[i]);
            if (i + 1 < zhengshu.length && zhengshu[i].equals("0") && zhengshu[i + 1].equals("0")) {
                tmp++;
                continue;
            }

            if (i == zhengshu.length - 1 && zhengshu[zhengshu.length - 1].equals("0")) {
                break;
            }

            if (zhengshu[i].equals("0")) {
                stringBuilder.append(num[nums]);
                tmp++;
            } else {
                stringBuilder.append(num[nums] + unit[3 - tmp]);
                tmp++;
            }
        }
    }
   
}
